PGDMP     "                    x            poli    11.5    11.5 #    !           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            "           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            #           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            $           1262    33949    poli    DATABASE     �   CREATE DATABASE poli WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Ecuador.1252' LC_CTYPE = 'Spanish_Ecuador.1252';
    DROP DATABASE poli;
             postgres    false            �            1259    33979    clientes    TABLE     
  CREATE TABLE public.clientes (
    id integer NOT NULL,
    cedula character varying(12),
    primernombre character varying(30),
    primerapellido character varying(30),
    direccion character varying(150),
    celular character varying(12),
    idrol integer
);
    DROP TABLE public.clientes;
       public         postgres    false            �            1259    33977    clientes_id_seq    SEQUENCE     �   CREATE SEQUENCE public.clientes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.clientes_id_seq;
       public       postgres    false    201            %           0    0    clientes_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.clientes_id_seq OWNED BY public.clientes.id;
            public       postgres    false    200            �            1259    33969    permisos    TABLE     ~   CREATE TABLE public.permisos (
    id integer NOT NULL,
    nombre character varying(30),
    codigo character varying(10)
);
    DROP TABLE public.permisos;
       public         postgres    false            �            1259    33967    permisos_id_seq    SEQUENCE     �   CREATE SEQUENCE public.permisos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.permisos_id_seq;
       public       postgres    false    199            &           0    0    permisos_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.permisos_id_seq OWNED BY public.permisos.id;
            public       postgres    false    198            �            1259    33961    roles    TABLE     {   CREATE TABLE public.roles (
    id integer NOT NULL,
    nombre character varying(30),
    codigo character varying(10)
);
    DROP TABLE public.roles;
       public         postgres    false            �            1259    33959    roles_id_seq    SEQUENCE     �   CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.roles_id_seq;
       public       postgres    false    197            '           0    0    roles_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;
            public       postgres    false    196            �            1259    34005    roles_permisos    TABLE     k   CREATE TABLE public.roles_permisos (
    id integer NOT NULL,
    idrol integer,
    idpermisos integer
);
 "   DROP TABLE public.roles_permisos;
       public         postgres    false            �            1259    34003    roles_permisos_id_seq    SEQUENCE     �   CREATE SEQUENCE public.roles_permisos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.roles_permisos_id_seq;
       public       postgres    false    203            (           0    0    roles_permisos_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.roles_permisos_id_seq OWNED BY public.roles_permisos.id;
            public       postgres    false    202            �
           2604    33982    clientes id    DEFAULT     j   ALTER TABLE ONLY public.clientes ALTER COLUMN id SET DEFAULT nextval('public.clientes_id_seq'::regclass);
 :   ALTER TABLE public.clientes ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201            �
           2604    33972    permisos id    DEFAULT     j   ALTER TABLE ONLY public.permisos ALTER COLUMN id SET DEFAULT nextval('public.permisos_id_seq'::regclass);
 :   ALTER TABLE public.permisos ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    199    198    199            �
           2604    33964    roles id    DEFAULT     d   ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);
 7   ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197            �
           2604    34008    roles_permisos id    DEFAULT     v   ALTER TABLE ONLY public.roles_permisos ALTER COLUMN id SET DEFAULT nextval('public.roles_permisos_id_seq'::regclass);
 @   ALTER TABLE public.roles_permisos ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    203    203                      0    33979    clientes 
   TABLE DATA               g   COPY public.clientes (id, cedula, primernombre, primerapellido, direccion, celular, idrol) FROM stdin;
    public       postgres    false    201   *%                 0    33969    permisos 
   TABLE DATA               6   COPY public.permisos (id, nombre, codigo) FROM stdin;
    public       postgres    false    199   G%                 0    33961    roles 
   TABLE DATA               3   COPY public.roles (id, nombre, codigo) FROM stdin;
    public       postgres    false    197   d%                 0    34005    roles_permisos 
   TABLE DATA               ?   COPY public.roles_permisos (id, idrol, idpermisos) FROM stdin;
    public       postgres    false    203   �%       )           0    0    clientes_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.clientes_id_seq', 1, false);
            public       postgres    false    200            *           0    0    permisos_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.permisos_id_seq', 1, false);
            public       postgres    false    198            +           0    0    roles_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.roles_id_seq', 1, false);
            public       postgres    false    196            ,           0    0    roles_permisos_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.roles_permisos_id_seq', 1, false);
            public       postgres    false    202            �
           2606    33984    clientes clientes_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.clientes DROP CONSTRAINT clientes_pkey;
       public         postgres    false    201            �
           2606    33974    permisos permisos_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.permisos
    ADD CONSTRAINT permisos_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.permisos DROP CONSTRAINT permisos_pkey;
       public         postgres    false    199            �
           2606    34010 "   roles_permisos roles_permisos_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.roles_permisos
    ADD CONSTRAINT roles_permisos_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.roles_permisos DROP CONSTRAINT roles_permisos_pkey;
       public         postgres    false    203            �
           2606    33966    roles roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public         postgres    false    197            �
           2606    33985    clientes clientes_idrol_fkey    FK CONSTRAINT     y   ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_idrol_fkey FOREIGN KEY (idrol) REFERENCES public.roles(id);
 F   ALTER TABLE ONLY public.clientes DROP CONSTRAINT clientes_idrol_fkey;
       public       postgres    false    201    197    2708            �
           2606    34016 -   roles_permisos roles_permisos_idpermisos_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles_permisos
    ADD CONSTRAINT roles_permisos_idpermisos_fkey FOREIGN KEY (idpermisos) REFERENCES public.permisos(id);
 W   ALTER TABLE ONLY public.roles_permisos DROP CONSTRAINT roles_permisos_idpermisos_fkey;
       public       postgres    false    2710    203    199            �
           2606    34011 (   roles_permisos roles_permisos_idrol_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles_permisos
    ADD CONSTRAINT roles_permisos_idrol_fkey FOREIGN KEY (idrol) REFERENCES public.roles(id);
 R   ALTER TABLE ONLY public.roles_permisos DROP CONSTRAINT roles_permisos_idrol_fkey;
       public       postgres    false    197    203    2708                  x������ � �            x������ � �            x������ � �            x������ � �     